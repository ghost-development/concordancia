import { Component, OnInit } from '@angular/core';
import { saveAs } from 'file-saver';
import Swal from 'sweetalert2'
import { HttpClient } from '@angular/common/http';

interface OutputConcordance {
  count: number;
  phrases: Array<number>;
}

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {

  public file: any;
  public loading = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  fileChanged(e) {
      this.file = e.target.files[0];
  }

  private getDictKeysAlphabetical(dict) {
    let keys = [], key;
    for (key in dict) {
      if (dict.hasOwnProperty(key)) keys.push(key);
    }
    keys.sort();
    return keys;
  }

  uploadDocument() {
    this.loading = true;
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.processData(fileReader.result);
    }
    fileReader.readAsText(this.file);
  }

  processData(data){
    // split input in array of lines
    data = data.split(/[\r\n]+/g); 
    // Count words repeat
    let output = {};
    let phrase = 1;
    data.forEach(line => {
      line = line.replace(/[^.,a-z,A-Z0-9 !?]/gm, " "); // Remove all special characters, except comma and dots.
      line.split(" ").forEach(element => {
        let elementFormatedClean: string = element.replace(/[^\w\s]/gi, ''); // Remove dots and commas to use element as key;
        if(elementFormatedClean){
          if(!output[elementFormatedClean]) output[elementFormatedClean] = {
            count: 0,
            phrases: []
          };

          output[elementFormatedClean]['count'] ++;
          output[elementFormatedClean]['phrases'].push(phrase);

          // if element has dot or comma break the phrase.
          if(elementFormatedClean != element) phrase ++;
        } 
      });
    });
    this.writeAndDownloadFile(output);
  }

  writeAndDownloadFile(output){
    // Example output line: a. bar: {1: 4}
    let line = 1;
    let blobText = "**-- CONCORDANCIA --*";
    this.getDictKeysAlphabetical(output).forEach(key => {
      let value: OutputConcordance = output[key];
      blobText += `\n${line}. ${key}: {${value.count}: ${value.phrases.join()}}`
      line ++;
    });
    blobText += "\n**-- FINAL DA CONCORDANCIA --*"
    let blob = new Blob([blobText],
    { type: "text/plain;charset=utf-8" });
    
    this.loading = false;
    Swal.fire({
      title: 'Success!!',
      text: 'Your processed file will be downloaded soon!',
      icon: 'success',
      confirmButtonText: 'Try Another Input'
    })
    saveAs(blob, "response.txt");
  }
}
